# **Raspberry Pi**

- Raspberry Pi is a low cost, miniature computer.
- It can be used to make multiple things from games to supercomputer.

## **Component Of Raspberry Pi**

![img2](images/img8.jpg)

### **1. Micro SD Card Slot**

- It is used to insert SD Card into Raspberry Pi. SD Card contains all the files and Raspberry Pi OS necessary to use Raspberry Pi.

### **2. HDMI Ports**

- It provide digital audio and video output. To view output of Raspberry Pi ,connect monitor/screen here.

### **3. Camera Module Jack**

- It is used to connect camera to Raspberry Pi for recording video.

### **4. Audio Jack**

- It provide analog audio output. Connect speaker or headphone here.

### **5. USB Port**

- It is used to connect mouse and keyboard to Raspberry Pi.

### **6. Ethernet Port**

- It is used to connect Raspberry Pi to the internet though ethernet cable. You can also connect Raspberry Pi to internet through wifi network.

### **7. GPIO Pins**

- GPIO stands for General Purpose Input Output. It is used to connect component like sensors, Led etc. to Raspberry Pi.

### **8. Micro USB Power Connector**

- Connect Raspberry Pi to power supply through this port. Always connect Raspberry Pi to the power supply, after connecting all other component to the Raspberry Pi.

![gif1](images/img9.gif)

### **Specification of Raspberry Pi**

| **Specification** | **Raspberry Pi 4** | **Raspberry Pi 3** |
| ------ | ------ |-------|
| Microprocessor | Broadcom BCM2711 64bit Quad Core | Broadcom BCM2837 64bit Quad Core |
| Processor Clock Speed | 1.4GHz | 1.2 GHz |
| Internal RAM | 4 GB | 1 GB|
| GPIO Pins | 40 | 40 |
| VIN | 5V | 5V |
| Operating Temp. | 0–50°C  | 0–50°C  |
