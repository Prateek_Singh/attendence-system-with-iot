# **20 Hour Learning**


- We can ```learn``` any new skill by ```practicing``` them. 
- According to research, when you are learning new skill your initial growth is rapid but when you become good in that field your growth versus time spent on practicing become linear(as shown in img1 and img 2) 

![img1](images/img_1.png)    ![img2](images/img2.png)


- Scientifically, it takes 1000 hours to ```master``` in any new field. But to be ```good``` in any field it just takes 20 hours.
 If you want to learn new language? want to learn how to draw? or any new skill. You can be good in any skill, if you spent only 20 Hours completely focused to practice it


## **To Gain New Skill In 20 Hours**

- To efficiently learn new skill in 20 hours, you have to follow 4 steps-

#### 1. Deconstruct The Skill

![img3](images/img3.png)

- If you want to learn new skill break it down into small action steps than try to learn the most important part first.

#### 2. Learn Enough To Self Correct

![img4](images/img4.png)

- Collect 4 to 5 resource material then learn just enough that you can actually practice in self correct manner.

#### 3. Remove Practice Barriers

![img5](images/img5.png)

- Remove all distraction while you are practicing that skill.

 
#### 4. Practice At Least 20 Hours

![img6](images/img6.png)

- Its the "Frustration Barrier" that holds up many people from learning something new. If you can practice 20 hours to learn new skill, you can cross this initial "Frustration Barrier".

