## **What is Git**

- Git is a version control software.
- It keeps record of the every changes done in project.
- It allows multiple people to work on same project by creating their own local copy.

## **Git Vocabulary**

- ### **Repository**
 A Repository is a collection of files and folder. It consists of the entire changes your team made in a project.

- ### **Commit**
 It is used to save the files created by you on your local machine.

- ### **Push**
 It is used to save the files from your local machine to the cloud.

- ### **Branch**
It is a branch of a master branch. It allows you to work on project without affecting master branch.

- ### **Merge**
It is used to merge your branch to the master branch.

- ### **Clone**
It create the exact copy of the online repository on your local machine.

- ### **Fork**
It is used to create entire new repo of the existing repo on your name.

- ### **Pull Request**
It is used to request for merging your changes into master branch.


## **Git Installation**

- For **_`Linux`_** , open terminal and type `sudo apt-get install git-all` .
- For **_`Windows`_** , [`download the installer`](https://git-scm.com/download/win) and run it.
- For **_`MAC`_** , open terminal and type `git` .

## **Git Workflow**

The basic git workflow goes like this-

|**S.No**| **Workflow Steps** | **Linux Command** |
|------| ------ | ------ |
| 1 | Clone the Repository | `$ git clone <link of the repository>` |
| 2 | Create new branch | `$ git checkout master` then `$ git checkout -b <branch-name>`|
| 3 | Modify file | `$ git add` |
| 4 | Commit changes | `$ git commit -sv` |
| 5 | Push changes | `$ git push origin <branch name>` |